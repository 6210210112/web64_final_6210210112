<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Firebase\JWT\JWT ;
$router->get('/booking', function() {
    $results = app('db')->select("SELECT * FROM booking");
    return response()->json($results);
});
$router->post('/login', function(\Illuminate\Http\Request $request) {
	$username = $request->input("username");
	$password = $request->input("password");

	$result = app('db')->select("SELECT UserID, password FROM login WHERE username=?",
									[$username]);
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "FAIL";
			$loginResult->reason = "User is not founded";
	}else {
		if(app('hash')->check($password, $result[0]->password)){
            $loginResult->status = "success";

			$payload = [
				'iss' => "runner_system",
				'sub' => $result[0]->UserID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
            ];

			$loginResult->token = JWT::encode($payload,env('APP_KEY'));

            return response()->json($loginResult);
			
		}else {
			$loginResult->status = "FAIL";
			$loginResult->reason = "Incorrect Password";
            return response()->json($loginResult);
		}
	}
	return response()->json($loginResult);
});
$router->post('/read_regist',function(Illuminate\Http\Request $request) {
    $name = $request->input("name");
    $surname = $request->input("surname");
    $username = $request->input("username");
    $password = app('hash')->make($request->input("password"));
    $phonenumber = $request->input("phonenumber");

    $query = app('db')->insert('INSERT into registration
                                       (name,surname,username,password,phonenumber) VALUES (?,?,?,?,?)',
                                       [$name,
                                       $surname,
                                       $username,
                                       $password,
                                       $phonenumber] );
   return "Read OK!" ;

});
$router->put('/update_regist',function(Illuminate\Http\Request $request) {
    $username = $request->input("username");
    $password = app('hash')->make($request->input("password"));
    $phonenumber = $request->input("phonenumber");
    $query = app('db')->update('UPDATE registration
                                        SET username=?,
                                            password=?
        
                                        WHERE 
      
                                              phonenumber =?',
                                              [ $username,
                                              $password,
                                              $phonenumber] );
   return "Update_OK" ;

});
$router->delete('/delete_regist',function(Illuminate\Http\Request $request) {
    $regID = $request->input("regID");

    $query = app('db')->delete('DELETE FROM registration
                                    WHERE
                                        regID=?',
                                       [$regID] );
    return "delete_OK" ;
});


